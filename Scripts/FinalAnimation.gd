extends Node

onready var game_controller = get_node("/root/Game/GameController")
var started = false
var ended = false

func _process(delta):
	if ended:
		if Input.is_key_pressed(KEY_ESCAPE):
			get_tree().change_scene("res://Scenes/MainScreen.tscn")

func _physics_process(delta):
	if ended:
		game_controller.camera.movement = Vector3(0.01, 0.05, 0)
	
func run_animation():
	if !started:
		started = true
		game_controller.player.set_active_control(false)
		game_controller.player.set_idle()
		game_controller.player.rotation_degrees = Vector3(0, 90, 0)
		game_controller.ui.get_dialog_system().type_dialog("Did you see the final last night?", "moss")
		game_controller.ui.get_dialog_system().type_dialog("F... off, Moss! Let's go back inside.", "roy")
		pass

func _on_Dialog_end_of_dialog(id):
	if started:
		print("ok")
		ended = true
		$FinalAnimationTimer.start()
		game_controller.player.set_active_control(false)
		#game_controller.camera.target = null
		get_node("/root/Game/GameObjects/Scenario/EndWall/CollisionShape").disabled = true
		
		game_controller.player.set_animation("Act_High_Five")
		game_controller.player.buddy.set_animation("Act_High_Five")
		
		yield( get_tree().create_timer(1.0), "timeout" )
		
		game_controller.player.buddy.active_control = true
		game_controller.player.run = true
		game_controller.player.buddy.run = true
		
		game_controller.player.move_to($FinalPosition.transform.origin)
		game_controller.player.disable_collision(true)

func _on_Player_automove_finished():
	game_controller.player.move_to(game_controller.player.transform.origin + Vector3(0, 0, -20))