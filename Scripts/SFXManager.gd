extends AudioStreamPlayer

const sfx = {
	"hit" : "res://Resources/sound/sfx/hit01.wav",
	"attack" : "res://Resources/sound/sfx/hit02.wav",
	"death" : "res://Resources/sound/sfx/death01.wav",
	"select" : "res://Resources/sound/sfx/select01.wav",
	"free_area" : "res://Resources/sound/sfx/siren03.wav",
	"wanted" : "res://Resources/sound/sfx/select07.wav",
	"kiss" : "res://Resources/sound/sfx/kiss01.wav",
}

func play_sfx(name):
	#set_stream(name)
	stream.audio_stream = load(sfx[name])
	play()
	
func stop_sfx():
	stop()