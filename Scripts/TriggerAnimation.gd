extends Spatial

var active = true
onready var game_controller = get_node("/root/Game/GameController")

func _on_Area_body_entered(body):
	if active && body.is_in_group("player"):
		active = false
		get_node("/root/Game/GameController/FinalAnimation").run_animation()