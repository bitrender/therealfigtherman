extends Node

var current_arena = null
var player
var camera
var buddy
var ui
var dynamic_objects

var game_paused = false
var game_over = false
var game_finished = false

func _ready():
	player = get_node("/root/Game/GameObjects/Player")
	camera = get_node("/root/Game/GameObjects/Camera")
	buddy = get_node("/root/Game/GameObjects/BuddyPlayer")
	ui = get_node("/root/Game/UI")
	dynamic_objects = preload("res://Nodes/DynamicObjects.tscn")
	
	#start first animation
	$StartAnimation.run_animation()

func _process(delta):
	get_inputs()

func _on_Arena_player_enter_arena(player, arena):
	current_arena = arena
	print("Você entrou na arena" + str(arena.arena_name))
	camera.target = arena

func _on_Arena_player_leave_arena(player, arena):
	current_arena = null
	print("Você saiu da arena" + str(arena.arena_name))
	camera.target = player
	
func get_inputs():
	if !game_over:
		if Input.is_action_just_pressed("pause"):
			game_paused = !game_paused
			pause_game(game_paused)
	else:
		if Input.is_action_just_pressed("restart"):
			restart()
			
	if Input.is_key_pressed(KEY_R):
		if ui.pause_screen.visible:
			restart()

func restart():
	get_tree().reload_current_scene()
	get_tree().paused = false

func pause_game(state):
	ui.show_pause_menu(state)
	get_tree().paused = state

func load_scene_objects():
	var objects = dynamic_objects.instance()
	get_node("/root/Game/GameObjects").add_child(objects)

func _on_Player_death():
	game_over = true
	$SFXManager.play_sfx("death")
	ui.show_game_over(true)

func _on_FinalAnimationTimer_timeout():
	ui.final_screen.start_animation()