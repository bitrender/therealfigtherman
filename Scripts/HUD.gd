extends Control

onready var _audio = get_node("/root/Game/GameController/SFXManager")

func _on_Player_health_update(value):
	$StatusContainer/HBoxContainer/VBoxContainer/LifeProgressBar.value = value
	#$StatusContainer/HBoxContainer/VBoxContainer/LifeProgressBar/Label.text = str(value)

func _on_Arena_arena_free():
	$MessageContainer/AnimationPlayer.play("Blink")
	_audio.play_sfx("free_area")


func _on_Player_wanted_level_update(value):
	$StatusContainer/HBoxContainer/HBoxContainer/VBoxContainer/WantedProgressBar.value = value
	$StatusContainer/HBoxContainer/HBoxContainer/VBoxContainer/WantedProgressBar/Label.text = str(value)
