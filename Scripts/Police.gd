extends Spatial

var player = null
onready var last_time = OS.get_ticks_msec()
var damage_delay = 1000
var damage = 2
var police_speed = 4

func _on_PoliceArea_body_entered(body):
	if body.name == "Player":
		player = body

func _on_PoliceArea_body_exited(body):
	if body.name == "Player":
		player = null
		
func _physics_process(delta):
	moving(delta)
	
	if player != null:
		if OS.get_ticks_msec() - last_time > damage_delay:
			player.add_wanted_level(damage)
			last_time = OS.get_ticks_msec()

func moving(delta):
	transform.origin.x -= police_speed * delta