extends KinematicBody

var movement = Vector3()
var run = false
var health = 10
var wanted_level = 0
var speed = 400
var enemy_target = null

var active_control = true

var automove_position = null

onready var follow_point = $FollowPoint
onready var buddy = get_node("/root/Game/GameController").buddy
onready var _audio = get_node("/root/Game/GameController/SFXManager")

signal health_update(value)
signal wanted_level_update(value)
signal death()
signal automove_finished()

enum STATE {dead, attacking, walk, run, attacked, idle, kissing, busted, high_five}
var state = STATE.idle

var attacked = false

var animation_state = {
	STATE.dead : "Act_Die",
	STATE.attacking : "Act_Punch.L",
	STATE.walk : "Act_Walk",
	STATE.run : "Act_Run",
	STATE.attacked : "Act_Punch_Impact.L",
	STATE.idle : "Act_Normal_Idle",
	STATE.kissing : "Act_Kiss_Moss",
	STATE.high_five: "Act_High_Five",
	STATE.busted: "Act_Busted",
}

func _ready():
	#register signals
	connect('health_update', get_node("/root/Game/UI/HUD"), '_on_Player_health_update')
	connect('wanted_level_update', get_node("/root/Game/UI/HUD"), '_on_Player_wanted_level_update')
	emit_signal("health_update", health)
	emit_signal("wanted_level_update", wanted_level)

func _process(delta):
	if state != STATE.dead:
		get_inputs()

func _physics_process(delta):
	if state != STATE.dead:
		
		#automove control
		if automove_position != null:
			print(automove_position)
			if transform.origin.distance_to(automove_position) < 1:
				automove_position = null
				emit_signal("automove_finished")
			else:
				#recalculate movement
				movement = (automove_position - transform.origin).normalized()
		
		
		if movement.length() > 0 && state != STATE.attacking && !attacked:
			if state == STATE.kissing:
				#disable kissing
				kissing(false)
			if run:
				move_and_slide(movement.normalized() * delta * speed * 2)
				state = STATE.run
			else:
				move_and_slide(movement.normalized() * delta * speed)
				state = STATE.walk
			
			update_animation()
			look_at(transform.origin + movement, Vector3(0, 1, 0))
		else:
			if state == STATE.run || state == STATE.walk:
				state = STATE.idle
				update_animation()

func get_inputs():
	movement = Vector3()
	if active_control:
		#movement
		if Input.is_key_pressed(KEY_A):
			movement.x = -1
		if Input.is_key_pressed(KEY_D):
			movement.x = 1
		if Input.is_key_pressed(KEY_W):
			movement.z = -1
		if Input.is_key_pressed(KEY_S):
			movement.z = 1
		
		#run
		if Input.is_key_pressed(KEY_SHIFT):
			run = true
			buddy.run = true
		else:
			run = false
			buddy.run = false
		
		#attack
		if Input.is_action_just_pressed("punch"):
			if state != STATE.kissing && state != STATE.attacking && !attacked:
				print(state)
				attack()
		
		#kissing
		if Input.is_action_just_pressed("kissing"):
			kissing(state != STATE.kissing)

func attack():
	state = STATE.attacking
	$PunchArea/AttackTimer.start()
	_audio.play_sfx("attack")
	update_animation()

func sucessful_attack():
	if enemy_target != null:
			var wr = weakref(enemy_target)
			if wr.get_ref():
				enemy_target.receive_damage(5)

func kissing(status):
	if status:
		_audio.play_sfx("kiss")
		look_at(buddy.get_global_transform().origin, Vector3(0, 1, 0))
		buddy.kiss(true)
		state = STATE.kissing
	else:
		_audio.stop_sfx()
		state = STATE.idle
		buddy.kiss(false)
	
	update_animation()

func receive_damage(value):
	if state != STATE.dead:
		#reset attacking timer
		$PunchArea/AttackTimer.stop()
		
		attacked = true
		$AttackedTimer.start()
		
		kissing(false)
		state = STATE.attacked
		update_animation()
		_audio.play_sfx("hit")
		health -= value
		emit_signal("health_update", health)
		if health <= 0:
			death()

func death(cause = null):
	if state != STATE.dead:
		state = STATE.dead
		
		if cause == "police":
			set_animation("Act_Busted")
		else:
			update_animation()
			
		buddy.queue_free()
		emit_signal("death")

func set_active_control(state):
	active_control = state
	get_node("/root/Game/GameController").buddy.active_control = state
	
	if !state:
		self.state = STATE.idle

func set_idle():
	state = STATE.idle;
	buddy.get_node("Scene Root/AnimationPlayer").play("Act_Normal_Idle")
	update_animation()

func play_animation(animation_name):
	if $"Scene Root/AnimationPlayer".current_animation != animation_name:
		$"Scene Root/AnimationPlayer".play(animation_name)

func update_animation():
	play_animation(animation_state[state])
	
func set_animation(animation):
	play_animation(animation)
	
func disable_collision(status):
	$CollisionShape.disabled = status

func move_to(position):
	automove_position = position

func add_wanted_level(value):
	if state != STATE.kissing:
		_audio.play_sfx("wanted")
		wanted_level = clamp(value + wanted_level, 0, 5)
		emit_signal("wanted_level_update", wanted_level)
		if wanted_level >= 5:
			death("police")

func _on_PunchArea_body_entered(body):
	if body.is_in_group("enemy"):
		enemy_target = body

func _on_PunchArea_body_exited(body):
	if body.is_in_group("enemy"):
		enemy_target = null

func _on_AttackTimer_timeout():
	sucessful_attack()

func _on_AnimationPlayer_animation_changed(old_name, new_name):
	if old_name == "Act_Punch.L":
		print("finished")
		state = STATE.idle

func _on_AttackedTimer_timeout():
	attacked = false
