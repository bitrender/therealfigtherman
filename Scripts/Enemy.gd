extends KinematicBody

var health = 10
var target
var dead = false
var attacking = false
var attacked = false

var enemy_target = false

onready var _audio = get_node("/root/Game/GameController/SFXManager")

signal death()

func _ready():
	target = get_node("/root/Game/GameController").player

func _physics_process(delta):
	if !dead && !attacked:
		var wr = weakref(target)
		if wr.get_ref():
			var dir = target.get_global_transform().origin - get_global_transform().origin
			dir = dir.normalized()
			var distance = target.get_global_transform().origin.distance_to(get_global_transform().origin)
			
			if distance > 3:
				move_and_slide(dir)
				look_at(target.get_global_transform().origin, Vector3(0, 1, 0))
					
				if $"Scene Root/AnimationPlayer".current_animation != "Act_Fighter_Walk":
					$"Scene Root/AnimationPlayer".play("Act_Fighter_Walk")
					pass
			else:
				if !attacking:
					attack()
			
func receive_damage(value = 1):
	if !dead:
		#reset the attacking timer
		$AttackingTimer.stop()
		$PunchArea/PunchDelayTimer.stop()
		
		$AttackedTimer.start()
		attacked = true
		print("ouch!")
		$"Scene Root/AnimationPlayer".play("Act_Punch_Impact.L")
		_audio.play_sfx("hit")
		health -= value
		if health <= 0:
			death()

func attack():
	attacking = true
	$AttackingTimer.start()
	$PunchArea/PunchDelayTimer.start()
	if $"Scene Root/AnimationPlayer".current_animation != "Act_Punch.L":
		$"Scene Root/AnimationPlayer".play("Act_Punch.L")

func death():
	dead = true
	emit_signal('death')
	$"Scene Root/AnimationPlayer".play("Act_Die")
	$CollisionShape.disabled = true
	#queue_free()

func _on_AttackedTimer_timeout():
	attacked = false
	attacking = false


func _on_AttackingTimer_timeout():
	attacking = false


func _on_PunchArea_body_entered(body):
	if body.is_in_group("player"):
		enemy_target = body


func _on_PunchArea_body_exited(body):
	if body.is_in_group("player"):
		enemy_target = null


func _on_PunchDelay_timeout():
	if enemy_target != null:
		enemy_target.receive_damage(2)
		print("bate")
