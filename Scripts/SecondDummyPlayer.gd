extends Spatial

var target
var velocity = 5
var kissing = false
var run = false
var active_control = true

func _ready():
	target = get_node("/root/Game/GameController").player;

func _physics_process(delta):
	if active_control:
		var wr = weakref(target)
		if wr.get_ref():
			if !kissing:
				#var offset = (target.get_global_transform().origin + target.transform.basis.z) - get_global_transform().origin
				var offset = target.follow_point.get_global_transform().origin - get_global_transform().origin
				transform.origin += offset * delta * velocity;
				var dir = target.get_global_transform().origin
				look_at(dir, Vector3(0, 1, 0))
				
				if transform.origin.distance_to(target.transform.origin) > 2.3:
					if run:
						if $"Scene Root/AnimationPlayer".current_animation != "Act_Run":
							set_animation("Act_Run")
					else:
						if $"Scene Root/AnimationPlayer".current_animation != "Act_Walk":
							set_animation("Act_Walk")
				else:
					if $"Scene Root/AnimationPlayer".current_animation != "Act_Normal_Idle":
						set_animation("Act_Normal_Idle")
		else:
			queue_free()

func kiss(state):
	if state:
		kissing = true
		set_animation("Act_Kiss_Roy")
		transform.origin = target.get_global_transform().origin - target.get_global_transform().basis.z
		var dir = target.get_global_transform().origin
		look_at(dir, Vector3(0, 1, 0))
	else:
		kissing = false
		set_animation("Act_Normal_Idle")
		
func set_animation(animation):
	$"Scene Root/AnimationPlayer".play(animation)