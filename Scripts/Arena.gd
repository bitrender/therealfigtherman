extends Spatial

export(String) var arena_name = "DefaultArena"
onready var enemy = preload("res://Nodes/DefaultEnemy.tscn")
onready var cop = preload("res://Nodes/Police.tscn")

export(int) var enemies_to_spawn = 2
var enemies_spawned = 0
var enemies_killed = 0
var started = false

signal player_enter_arena(player, arena)
signal player_leave_arena(player, arena)
signal arena_free()

func _ready():
	connect('player_leave_arena', get_node("/root/Game/GameController"), '_on_Arena_player_leave_arena')
	connect('player_enter_arena', get_node("/root/Game/GameController"), '_on_Arena_player_enter_arena')
	connect('arena_free', get_node("/root/Game/UI/HUD"), '_on_Arena_arena_free')

func _on_ArenaArea_body_entered(body):
	if !started:
		if body.is_in_group("player"):
			emit_signal("player_enter_arena", body, self)
			started = true
			$Block/StaticBody/EnterCollision.disabled = false
			$SpawnerTimer.start()
			$CopsTimer.start()
			spawn_enemy()

func _on_ArenaArea_body_exited(body):
	if body.name == "Player":
		emit_signal("player_leave_arena", body, self)

func spawn_enemy(n = 1):
	for i in range(n):
		var new_enemy = enemy.instance()
		if randf() < 0.5:
			new_enemy.transform.origin = Vector3(22, 0, 0)
		else:
			new_enemy.transform.origin = Vector3(-22, 0, 0)
			
		new_enemy.transform.origin += (Vector3(0, 0, rand_range(-10, 10)))
		add_child(new_enemy)
		new_enemy.connect('death', self, '_on_Enemy_death')
		enemies_spawned += 1

func spawn_cops():
	var new_cop = cop.instance()
	new_cop.transform.origin = Vector3(25, 0, 0)
	add_child(new_cop)

func _on_Enemy_death():
	enemies_killed += 1
	if enemies_killed >= enemies_to_spawn:
		print('Liberar!')
		$Block/StaticBody/ExitCollision.disabled = true
		emit_signal("arena_free")

func _on_SpawnerTimer_timeout():
	if enemies_spawned < enemies_to_spawn:
		spawn_enemy()
	else:
		$SpawnerTimer.stop()
		print("Spawner Stopped")

func _on_CopsTimer_timeout():
	spawn_cops()
