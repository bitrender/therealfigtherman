extends Node

onready var game_controller = get_node("/root/Game/GameController")
var started = false
var ended = false

func run_animation():
	if !started:
		started = true
		game_controller.player.set_active_control(false)
		game_controller.player.rotation_degrees = Vector3(0, 90, 0)
		game_controller.ui.get_dialog_system().type_dialog("I need to know how much is there.", "moss")
		game_controller.ui.get_dialog_system().type_dialog("No, you don't. We need to get out of here, OK? \nWe do not want to get mixed up in this.", "roy")
		
		game_controller.ui.get_dialog_system().type_dialog("Oh, f... What are we gonna do now?", "roy")
		game_controller.ui.get_dialog_system().type_dialog("Quick! (Press E to hide yourself)", "moss")
	
func stop_animation():
	if !ended:
		ended = true
		game_controller.player.rotation_degrees = Vector3(0, -90, 0)
		game_controller.load_scene_objects()
		game_controller.player.set_active_control(true)

func _on_Dialog_end_of_dialog(id):
	stop_animation()