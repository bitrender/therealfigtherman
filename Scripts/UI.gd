extends CanvasLayer

onready var pause_screen = $PauseScreen
onready var final_screen = $FinalScreen

func show_pause_menu(state):
	$PauseScreen.visible = state
	
func get_dialog_system():
	return $Dialog
	
func show_game_over(state):
	$GameOverScreen.visible = state