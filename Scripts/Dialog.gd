extends Control

var sentences_queue = Array()
onready var label = $Panel/HBoxContainer/Panel/Label
onready var photo = $Panel/HBoxContainer/TextureRect
onready var _audio = get_node("/root/Game/GameController/SFXManager")

signal end_of_dialog(id)

var character_photo = {
	"moss" : "res://Resources/textures/moss_photo.png",
	"roy" : "res://Resources/textures/roy_photo.png"
	}

func _process(delta):
	get_inputs()
	
	if !sentences_queue.empty():
		if !visible:
			run_dialog()

func get_inputs():
	if visible:
		if Input.is_action_just_pressed("skip"):
			_audio.play_sfx("select")
			skip_dialog()

func run_dialog():
	if !sentences_queue.empty():
		visible = true
		#show sentence
		label.text = sentences_queue[0]["sentence"]
		var url = character_photo[sentences_queue[0]["character"]]
		photo.texture = load(url)
	else:	
		emit_signal("end_of_dialog", null)
		print("dialog run over")
		visible = false

func skip_dialog():
	sentences_queue.remove(0)
	run_dialog()

func type_dialog(sentence, character = null):
	sentences_queue.push_back({"sentence" : sentence, "character" : character})