extends Control

func _ready():
	get_tree().paused = false

func _on_StartGameButton_pressed():
	get_tree().change_scene("res://Scenes/TesteScene.tscn")

func _on_ExitButton_pressed():
	get_tree().quit()