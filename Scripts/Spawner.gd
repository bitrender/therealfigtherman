extends Spatial

export(int) var enemies_to_spawn = 2
export(int) var cops_to_spawn = 0
export(bool) var spawn_active = true

onready var enemy = preload("res://Nodes/DefaultEnemy.tscn")
onready var cop = preload("res://Nodes/Police.tscn")

func _on_Area_body_entered(body):
	if spawn_active && body.is_in_group("player"):
		spawn_active = false
		spawn_enemies()
		spawn_cops()

func spawn_enemies():
	for i in range(enemies_to_spawn):
			var new_enemy = enemy.instance()
			new_enemy.transform.origin += (Vector3(rand_range(-10, 0), 0, rand_range(-10, 10)))
			$Target.add_child(new_enemy)
			
func spawn_cops():
	for i in range(cops_to_spawn):
			var new_cop = cop.instance()
			$Target.add_child(new_cop)