extends Camera

var target
var z_movement = false
var movement = null

func _ready():
	target = get_node("/root/Game/GameObjects/Player")

func _process(delta):
	if movement != null:
		transform.origin += movement
	else:
		if target != null:
			var wr = weakref(target)
			if wr.get_ref():
				var offset_x = (target.transform.origin.x - transform.origin.x) * 0.1
				if transform.origin.x + offset_x > -28:
					transform.origin.x += offset_x
					if z_movement:
						transform.origin.z += (target.transform.origin.z - transform.origin.z + 20) * 0.05